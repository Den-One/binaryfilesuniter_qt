#ifndef CRC32CHECKER_H
#define CRC32CHECKER_H

#include <QFile>
#include <QMessageBox>
#include <QIcon>

class Crc32Checker
{
public:
    Crc32Checker();
    ~Crc32Checker();
    void init();

public slots:
    uint32_t findCrc32InSended(QString filePath);
    uint32_t findCrc32InRecieved(QString filePath);
    bool isCrc32Equal(uint32_t crc1, uint32_t crc2);

    void culculateCrc(QString filePath);

private slots:
    uint32_t getCrc32(uint8_t* data, int length);
    QByteArray getFileContent(QString filePath);

private:
    QByteArray _sendedCrc32;
    QByteArray _recievedCrc32;
};

#endif // CRC32CHECKER_H
