#ifndef UNITINGPARSER_H
#define UNITINGPARSER_H

#include "filemanager.h"

#include <iostream>

class UnitingParser
{
public:
    UnitingParser(int argc, QList<QString> argv);
    ~UnitingParser();

    void process();

private:
    bool hasCorrectOption();
    bool hasBootLoaderOption();
    bool hasFirmwareOption();
    bool hasHelpOption();

    bool hasCorrectPath(QString argv);
    bool hasCorrectDirectory(QString argv);

    QString toQString(QString argv);

private:

    QList<QString> _options;
    int _argcN = 0;
    QList<QString> _argvs;
};

#endif // UNITINGPARSER_H
