#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "QFileDialog"
#include "filemanager.h"
#include "fileId.h"
#include "crc32checker.h"

class FileManager;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void setPath(QString str);

public slots:

private slots:

    void onChooseFileButtonClicked();
    void onMakeFileButtonClicked();

    void onPushButtonPressed();
    void onPushButtonReleased();

private:
    void showFilePath(QString filePath);

    Ui::MainWindow *_ui;
    FileManager *_fileManager;
    QList<QPushButton*> _uiButtons;

    QString _STYLE = "border-radius:25px;"
                     "border:50px;"
                     "color: rgb(187, 225, 250);";

    QString _PRESED_COLOR = "background-color: rgb(27, 38, 44);";
    QString _RELEASED_COLOR = "background-color: rgb(5, 76, 117);";

};
#endif // MAINWINDOW_H
