#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "fileId.h"

#define IN_GUI_MODE false

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , _ui(new Ui::MainWindow)

{
    _ui->setupUi(this);
    _fileManager = new FileManager(IN_GUI_MODE);
    // can't change window size
    setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);

    connect(_ui->chooseButton1, &QPushButton::clicked,
            this, &MainWindow::onChooseFileButtonClicked);
    connect(_ui->chooseButton2, &QPushButton::clicked,
            this, &MainWindow::onChooseFileButtonClicked);
    connect(_ui->makeFileButton, &QPushButton::clicked,
            this, &MainWindow::onMakeFileButtonClicked);

    _uiButtons = centralWidget()->findChildren<QPushButton*>();
    for(const auto button : qAsConst(_uiButtons)) {
        connect(button, &QPushButton::pressed
                , this, &MainWindow::onPushButtonPressed);
        connect(button, &QPushButton::released
                , this, &MainWindow::onPushButtonReleased);
    }
}

MainWindow::~MainWindow()
{
    delete _ui;
    delete _fileManager;

    disconnect(_ui->chooseButton1, &QPushButton::clicked,
               this, &MainWindow::onChooseFileButtonClicked);
    disconnect(_ui->chooseButton2, &QPushButton::clicked,
               this, &MainWindow::onChooseFileButtonClicked);
    disconnect(_ui->makeFileButton, &QPushButton::clicked,
               this, &MainWindow::onMakeFileButtonClicked);

    for(const auto button : qAsConst(_uiButtons)) {
        disconnect(button, &QPushButton::pressed
                   , this, &MainWindow::onPushButtonPressed);
        disconnect(button, &QPushButton::released
                   , this, &MainWindow::onPushButtonReleased);
    }
}

void MainWindow::onChooseFileButtonClicked() {
    QString path = "";
    QString title = "File Searching";
    QString dir = _fileManager->getLastFileDir();

    QPushButton* target = qobject_cast<QPushButton*>(sender());
    if (!target) return;   
    switch (target->accessibleName().toInt()) {
        case FileId::FIRST:
            path = QFileDialog::getOpenFileName(nullptr, title, dir, "*.bin");
            _fileManager->setFile1Path(path);
            _ui->path->setText(_fileManager->getFile1Path());
            break;
        case FileId::SECOND:
            path = QFileDialog::getOpenFileName(nullptr, title, dir, "*.bin");
            _fileManager->setFile2Path(path);
            _ui->path->setText(_fileManager->getFile2Path());
            auto crc32Checker = std::make_unique<Crc32Checker>();
            crc32Checker->culculateCrc(path);
            break;
    }
}

void MainWindow::onMakeFileButtonClicked() {
    QString path = _fileManager->getFile2Path();
    QByteArray file2Content = _fileManager->getFileContent(path);
    path = _fileManager->getFile1Path();
    QByteArray file1Content = _fileManager->getFileContent(path);
    QByteArray content = _fileManager->sumContent(file1Content, file2Content);

    QString title = "Searching Place For New File";
    path = QFileDialog::getExistingDirectory(nullptr, title, "");
    _ui->path->setText(path);
    QString fileName = _fileManager->generateFileName(file2Content);
    _fileManager->makeFile(path, content, fileName);
}

void MainWindow::onPushButtonPressed() {
    QPushButton* target = static_cast<QPushButton*>(sender());
    QString clickedButtonName = target->objectName();

    if (!target) return;
    QString buttonStyle = "#%1 {" + _STYLE + _PRESED_COLOR + "}";
    target->setStyleSheet(QString(buttonStyle).arg(clickedButtonName));
}

void MainWindow::onPushButtonReleased() {
    QPushButton* target = static_cast<QPushButton*>(sender());
    QString clickedButtonName = target->objectName();

    if (!target) return;
    QString buttonStyle = "#%1 {" + _STYLE + _RELEASED_COLOR + "}";
    target->setStyleSheet(QString(buttonStyle).arg(clickedButtonName));
}

void MainWindow::setPath(QString str) {
    _ui->path->setText(str);
}
