#include "mainwindow.h"
#include "unitingparser.h"

#include <QApplication>
#include <QLockFile>
#include <QCommandLineParser>
#include <QDebug>

#include <stdio.h>
#include <windows.h>

#include "unitingparser.h"

int main(int argc, char *argv[]) {

    if (argc == 1) {
        HWND hConsole = GetConsoleWindow();
             ShowWindow(hConsole, SW_HIDE);

        QApplication a(argc, argv);
        MainWindow w(nullptr);
        QIcon icon(":/png/pictures/appIcon.ico");
        w.setWindowIcon(icon);
        w.show();

        return a.exec();
    } else {

        QList<QString> argvs = {};
        for (int i = 0; i < argc; ++i) {
            argvs.push_back(argv[i]);
        }

        UnitingParser parser(argc, argvs);
        parser.process();
    }

    return 0;
}
