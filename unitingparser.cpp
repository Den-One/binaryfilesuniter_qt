#include "unitingparser.h"

#define CONSOLE_MODE true

UnitingParser::UnitingParser(int argc, QList<QString> argv) {
    _argcN = argc;

    for (int i = 0; i < argv.size(); ++i)
        _argvs.push_back(argv[i]);

    _options.push_back("-h");
    _options.push_back("--help");
    _options.push_back("-bp");
    _options.push_back("--bootloader-path");
    _options.push_back("-fp");
    _options.push_back("--firmware-path");
}

UnitingParser::~UnitingParser() {
}

void UnitingParser::process() {

    if (!hasCorrectOption())
        std::cout << "Incorrect argument. Try -h or --help\n";
    if (hasHelpOption())
        std::cout << "This application unites two bin-files.\n"
                  << "To start working, input:\n"
                  << "--bootloader-path file_path --firmware-path file_path dir_path\n"
                  << "or\n"
                  << "-bp file_path -fp file_path dir_path\n\n"
                  << "file_path: path of file with suffix .bin\n"
                  << "dir_path: path of directory to save new file\n\n"
                  << "example: -bp C:\\Users\\user\\Documents\file1name.bin "
                  << "-fp C:\\Users\\user\\Documents\file2name.bin "
                  << "C:\\Users\\user\\Documents\n";

    if (hasBootLoaderOption()) {
        if (_argcN > 2 && (!hasCorrectPath(_argvs[2]))) {
            _argvs[2] += ".bin";
            if (!hasCorrectPath(_argvs[2]))
                std::cout << "Incorrect file path:\n" << _argvs[2].toStdString() << "\n";
        } else
            std::cout << "There is no path after bootloader option";

        if (hasFirmwareOption()) {
            if (_argcN > 4 && (!hasCorrectPath(_argvs[4]))) {
                _argvs[4] += ".bin";
                if (!hasCorrectDirectory(_argvs[4]))
                    std::cout << "Incorrect file path:\n" << _argvs[4].toStdString() << "\n";
            }
            else
                std::cout << "There is no path after bootloader option";

            if (!hasCorrectDirectory(_argvs[5]))
                std::cout << "Incorrect directory:\n" << _argvs[5].toStdString() << "\n";
            else {
                std::unique_ptr<FileManager> _fileManager;
                _fileManager = std::make_unique<FileManager>(CONSOLE_MODE);
                _fileManager->setFile1Path(_argvs[2]);
                _fileManager->setFile2Path(_argvs[4]);
                QString path = _argvs[4];
                QByteArray file2Content = _fileManager->getFileContent(path);
                path = _argvs[2];
                QByteArray file1Content = _fileManager->getFileContent(path);
                QByteArray content = _fileManager->sumContent(file1Content, file2Content);

                QString fileName = _fileManager->generateFileName(file2Content);
                path = _argvs[5];
                _fileManager->makeFile(path, content, fileName);
            }
        }
    } else {
        std::cout << "Incorrect option. Try -h or --help." << "\n";
    }
}

bool UnitingParser::hasCorrectOption() {
    if (_argcN > 1 && _options.contains(_argvs[1]))
        return true;
    else
        return false;
}

bool UnitingParser::hasBootLoaderOption() {
    if (_argcN > 1 && (_argvs[1] == "-bp" || _argvs[1] == "--bootloader-path"))
        return true;
    else
        return false;
}

bool UnitingParser::hasFirmwareOption() {
    if (_argcN > 3 && (_argvs[3] == "-fp" || _argvs[3] == "--firmware-path"))
        return true;
    else
        return false;
}

bool UnitingParser::hasHelpOption() {
    if (_argcN > 1 && (_argvs[1] == "-h" || _argvs[1] == "--help"))
        return true;
    else
        return false;
}

bool UnitingParser::hasCorrectPath(QString argv) {
    return QFile::exists(argv);
}

bool UnitingParser::hasCorrectDirectory(QString argv) {
    QDir dir;
    return (dir.exists(argv));
}
