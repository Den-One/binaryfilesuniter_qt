#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include "fileId.h"
#include "QFileDialog"

#include <QMessageBox>

class FileManager : public QObject
{
    Q_OBJECT

public:
    FileManager(bool inConsoleMode);
    virtual ~FileManager();

public slots:
    void setFile1Path(const QString& path);
    void setFile2Path(const QString& path);

    QString getFile1Path();
    QString getFile2Path();

    QByteArray getFileContent(QString& filePath);

    void makeFile(QString &path, QByteArray &content, QString name);
    QByteArray sumContent(QByteArray &data1, QByteArray &data2);

    QString getLastFileDir();
    QString generateFileName(QByteArray& fileContent);

private slots:
    void makeWindow(QMessageBox::Icon i, QString message);
    void showMessage(QMessageBox::Icon i, QString message);
    void printToConsole(QString message);

    void makeDivSaver();
    void saveLastFileDir(QString path);

    QByteArray findVersionNumber(QByteArray &fileContent);

private:
    std::unique_ptr<QFile> _file;
    std::unique_ptr<QFile> _dirSaver;

    QString _file1Path = "";
    QString _file2Path = "";

    const QString _SUFFIX = ".bin";
    const QByteArray _CONSOLE_SIGNATURE = "MCONS_FW";

    bool _inConsoleMode;
};

#endif // FILEMANAGER_H
