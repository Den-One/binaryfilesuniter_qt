#include "filemanager.h"
#include <QMessageBox>

#include <QIcon>
#include <iostream>

FileManager::FileManager(bool inConsoleMode) {
    makeDivSaver();
    _inConsoleMode = inConsoleMode;
}

FileManager::~FileManager() {
}

void FileManager::setFile1Path(const QString& path) {
    _file1Path = path;
    saveLastFileDir(path);
}

void FileManager::setFile2Path(const QString& path) {
    _file2Path = path;
    saveLastFileDir(path);
}

QString FileManager::getFile1Path() {
    return _file1Path;
}

QString FileManager::getFile2Path() {
    return _file2Path;
}

QByteArray FileManager::getFileContent(QString& filePath) {
    QByteArray fileContent = "";
    _file = std::make_unique<QFile>(filePath);
    if (!_file->exists())
        showMessage(QMessageBox::Critical, "File wasn't chosen.");
    else {
        _file->open(QIODevice::ReadOnly);
        fileContent = _file->readAll();
        _file->close();
    }

    return fileContent;
}

void FileManager::makeFile(QString &path, QByteArray &content, QString name) {
    _file = std::make_unique<QFile>(path + "\\" + name + _SUFFIX);
    if (!_file->open(QIODevice::WriteOnly))
        showMessage(QMessageBox::Critical, "Can't make file in a directory");
    else {
        _file->write(content);
        _file->close();
        showMessage(QMessageBox::Information, "File was made successfully");
    }

    saveLastFileDir(path);
}

QByteArray FileManager::sumContent(QByteArray &data1, QByteArray &data2) {
    QByteArray sumContent = data1 + data2;
    return sumContent;
}

void FileManager::makeDivSaver() {
    _dirSaver = std::make_unique<QFile>("lastPath.txt");
    if (!_dirSaver->exists()) {
        if (!_dirSaver->open(QIODevice::WriteOnly))
            showMessage(QMessageBox::Critical, "Can't make path saver");
        else
            _dirSaver->close();
    }
}

void FileManager::saveLastFileDir(QString path) {
    QString dir = "";
    size_t pathHasFileName = path.toStdString().find(".");
    if (pathHasFileName != std::string::npos) {
        dir = path.split(".")[0];
        for (int i = dir.size(); i > 0; --i) {
            if (dir[i-1] == '/' || (dir[i-1] == '\\'))
                break;
            else
                dir.resize(i-1);
        }
    }
    else
        dir = path;

    if (dir != "") {
        if (!_dirSaver->open(QIODevice::WriteOnly))
            showMessage(QMessageBox::Critical, "Can't open path saver");
        else {
            _dirSaver->write(dir.toStdString().c_str());
            _dirSaver->close();
        }
    }
}

QString FileManager::getLastFileDir() {
    QString lastOpenDir = "";
    if (_dirSaver->exists()) {
        _dirSaver->open(QIODevice::ReadOnly);
        lastOpenDir = _dirSaver->readAll();
        _dirSaver->close();
    }
    return lastOpenDir;
}

QString FileManager::generateFileName(QByteArray& fileContent) {

    QByteArray fileName = "Mcons20_fabr";
    QByteArray version = findVersionNumber(fileContent);
    for (int byte = 0; byte < version.size(); ++byte) {
        int i = version[byte];
        char smbl = i + '0';
        if (smbl <= '9' && i <= 9) {
            fileName = fileName + "_" + smbl;
        }
        else {
            char buffer [33] {};
            itoa(i + '0',buffer,16);
            fileName = fileName + "_";
            for (int k = 0; k < 33; ++k) {
                if (buffer[k] != '\0')
                    fileName += buffer[k];
                else
                    break;
            }
        }

    }

    QString res = fileName.toStdString().c_str();
    return res;
}

void FileManager::makeWindow(QMessageBox::Icon i, QString message) {
    QMessageBox msgBox;
    msgBox.setIcon(i);
    QIcon icon(":/png/pictures/appIcon.ico");
    msgBox.setWindowIcon(icon);
    msgBox.setText(message);
    msgBox.setWindowTitle("Message");
    msgBox.exec();
}

void FileManager::showMessage(QMessageBox::Icon i, QString message) {
    _inConsoleMode ?printToConsole(message) :makeWindow(i, message);
}

void FileManager::printToConsole(QString message) {
    std::cout << message.toStdString() << std::endl;
}

QByteArray FileManager::findVersionNumber(QByteArray &fileContent) {
    QByteArray _versionNumber = "";
    int match(0), save(0);
    // look for the last iterator of substring in a string
    for (int i(0), j(0); i < fileContent.size(); ++i) {
        if (fileContent[i] == _CONSOLE_SIGNATURE[j])
            for (; j < _CONSOLE_SIGNATURE.size(); ++j, ++i) {
                if (fileContent[i] == _CONSOLE_SIGNATURE[j]) {
                    match++;
                    if (match == _CONSOLE_SIGNATURE.size()) {
                        save = i;
                        break;
                    }
                } else {
                    match = 0;
                    j = 0;
                    break;
                }
            }

        if (save != 0) break;
    }

    for (int nextNum = 1; nextNum < 4; ++nextNum)
        _versionNumber += fileContent[save + nextNum];

    return _versionNumber;
}
